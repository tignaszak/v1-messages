FROM openjdk:19-alpine
ADD messaging/build/libs/*.jar manager_messages_v1_app.jar
ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/manager_messages_v1_app.jar"]
EXPOSE 8080
EXPOSE 5005