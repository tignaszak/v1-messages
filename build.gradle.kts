import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version Conf.Versions.SPRING_BOOT
    id("io.spring.dependency-management") version Conf.Versions.DEPENDENCY_MANAGEMENT

    kotlin("jvm") version Conf.Versions.KOTLIN
    kotlin("plugin.spring") version Conf.Versions.KOTLIN
    kotlin("kapt") version Conf.Versions.KOTLIN
}

java.sourceCompatibility = JavaVersion.VERSION_18
java.targetCompatibility = JavaVersion.VERSION_18

springBoot {
    mainClass.set("net.ignaszak.manager.messages.MessagesApplicationKt")
}

allprojects {
    group = Conf.GROUP
    version = Conf.VERSION

    repositories {
        mavenCentral()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = Conf.Versions.JAVA
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()

        testLogging {
            events("passed", "skipped", "failed")
            exceptionFormat = FULL
            showCauses = true
            showExceptions = true
            showStackTraces = true
        }

        afterSuite(KotlinClosure2<TestDescriptor, TestResult, Unit>({desc, result ->
            if (desc.parent != null) {
                val output = result.run {
                    "${desc.name} results: $resultType ($testCount tests, $successfulTestCount successes, " +
                            "$failedTestCount failures, $skippedTestCount skipped)"
                }
                println(output)
            }
            Unit
        }))
    }
}

subprojects {
    repositories {
        mavenCentral()
        maven {
            setUrl("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }

    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "groovy")

    dependencies {
        implementation(Conf.Deps.GSON)
        implementation(Conf.Deps.JACKSON_MODULE_KOTLIN)
        implementation(Conf.Deps.KOTLIN_REFLECT)
        implementation(Conf.Deps.KOTLIN_STDLIN_JDK8)
        implementation(Conf.Deps.SNAKEYAML)

        testImplementation(Conf.Deps.GROOVY)
        testImplementation(Conf.Deps.SPOCK_CORE)
    }
}

project(":messaging") {
    tasks.jar {
        enabled = false
    }

    apply(plugin = "org.springframework.boot")
    apply(plugin = "kotlin-kapt")

    dependencies {
        implementation(project(":model"))
        implementation(project(":service"))
        implementation(project(":servicecontract"))

        implementation(Conf.Deps.MANAGER_COMMONS_CONF)
        implementation(Conf.Deps.MANAGER_COMMONS_MESSAGING)

        implementation(Conf.Deps.MAPSTRUCT)
        implementation(Conf.Deps.SPRING_BOOT)
        implementation(Conf.Deps.SPRING_BOOT_STARTER_AMQP)
        implementation(Conf.Deps.SPRING_BOOT_STARTER_WEB)
        implementation(Conf.Deps.SPRING_CLOUD_CONFIG_BOOTSTRAP)
        implementation(Conf.Deps.SPRING_CLOUD_STARTER_CONFIG)
        implementation(Conf.Deps.SPRING_CLOUD_STARTER_NETFLIX_EUREKA_CLIENT)

        testImplementation(Conf.Deps.MANAGER_COMMONS_ERROR)
        testImplementation(Conf.Deps.MANAGER_COMMONS_TEST)
        testImplementation(Conf.Deps.SPRING_BOOT_STARTER_TEST)
        testImplementation(Conf.Deps.TESTCONTAINERS)

        kapt(Conf.Deps.MAPSTRUCT_PROCESSOR)
    }
}

project(":servicecontract") {
    dependencies {
        implementation(project(":model"))
    }
}

project(":service") {
    dependencies {
        implementation(project(":model"))
        implementation(project(":servicecontract"))

        implementation(Conf.Deps.SPRING_BOOT_STARTER_MAIL)
        implementation(Conf.Deps.SPRING_CONTEXT)
    }
}

project(":model") {
}