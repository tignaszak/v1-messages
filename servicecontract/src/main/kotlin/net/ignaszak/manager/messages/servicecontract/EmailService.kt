package net.ignaszak.manager.messages.servicecontract

import net.ignaszak.manager.messages.model.EmailModel

interface EmailService {

    fun send(email: EmailModel)
}