package net.ignaszak.manager.messages.model

data class EmailModel(
        val to: String,
        val from: String,
        val subject: String,
        val body: String
)