package net.ignaszak.manager.messaging.integration

import net.ignaszak.manager.commons.messaging.message.EmailMessage
import net.ignaszak.manager.commons.test.specification.IntegrationSpecification
import net.ignaszak.manager.messages.MessagesApplication
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.GenericContainer
import spock.lang.Shared

import static org.springframework.http.HttpMethod.DELETE
import static org.springframework.http.HttpStatus.OK
import static org.springframework.util.CollectionUtils.isEmpty

@ContextConfiguration(classes = MessagesApplication.class)
class MessagingIntegrationSpec extends IntegrationSpecification {

    private static int SMTP_API_PORT

    @Shared
    private static final GenericContainer SMTP_CONTAINER = new GenericContainer("reachfive/fake-smtp-server")
            .withExposedPorts(1025, 1080)

    @DynamicPropertySource
    private static void rabbitmqProperties(DynamicPropertyRegistry registry) {
        SMTP_CONTAINER.start()

        registry.add("spring.mail.host", SMTP_CONTAINER::getContainerIpAddress)
        registry.add("spring.mail.port", () -> SMTP_CONTAINER.getMappedPort(1025))

        SMTP_API_PORT = SMTP_CONTAINER.getMappedPort(1080)
    }

    @Autowired
    private RabbitTemplate rabbitTemplate

    def cleanup() {
        removeEmails()
    }

    def cleanupSpec() {
        SMTP_CONTAINER.stop()
    }

    def "send email"() {
        given:
        def to = "to@email.com"
        def from = "from@email.com"
        def subject = "Email subject"
        def body = "Email body"
        def emailMessage = new EmailMessage(to, from, subject, body)

        when:
        rabbitTemplate.convertAndSend(
                managerProperties.jms.queues.email.exchange,
                managerProperties.jms.queues.email.routingKey,
                emailMessage
        )

        then:
        def responseEntity = getEmailResponseEntity("http://localhost:${SMTP_API_PORT}/api/emails?from=${from}&to=${to}")
        responseEntity.statusCode == OK
        responseEntity.body.size() == 1
        responseEntity.body.get(0).subject == subject
        responseEntity.body.get(0).text.startsWith(body)
    }

    def "should not send email with incorrect data"() {
        given:
        def emailMessage = new EmailMessage("", "", "", "")

        when:
        rabbitTemplate.convertAndSend(
                managerProperties.jms.queues.email.exchange,
                managerProperties.jms.queues.email.routingKey,
                emailMessage
        )

        then:
        def responseEntity = getEmailResponseEntity("http://localhost:${SMTP_API_PORT}/api/emails")
        responseEntity.statusCode == OK
        isEmpty(responseEntity.body)
    }

    private ResponseEntity<Void> removeEmails() {
        restTemplate.exchange("http://localhost:${SMTP_API_PORT}/api/emails", DELETE, null, Void.class)
    }

    private ResponseEntity<List<Map<String, String>>> getEmailResponseEntity(String url) {
        def responseEntity
        def count = 0

        do {
            count++
            Thread.sleep(500)
            responseEntity = restTemplate.getForEntity(url, Object.class) as ResponseEntity<List<Map<String, String>>>
        } while (count <= 5 && (responseEntity == null || isEmpty(responseEntity.body)))

        return responseEntity
    }
}
