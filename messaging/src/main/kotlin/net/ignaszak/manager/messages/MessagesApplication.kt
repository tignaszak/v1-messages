package net.ignaszak.manager.messages

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@EnableEurekaClient
@EnableRabbit
@EnableConfigurationProperties(ManagerProperties::class)
@ComponentScan(
    "net.ignaszak.manager.messages",
    "net.ignaszak.manager.commons.conf"
)
open class MessagesApplication

fun main(args: Array<String>) {
    runApplication<MessagesApplication>(*args)
}