package net.ignaszak.manager.messages.messaging.email.mapper

import net.ignaszak.manager.commons.messaging.message.EmailMessage
import net.ignaszak.manager.messages.model.EmailModel
import org.mapstruct.Mapper
import org.springframework.stereotype.Component

@Component
@Mapper(componentModel = "spring")
interface EmailMapper {

    fun messageToModel(emailMessage: EmailMessage): EmailModel
}