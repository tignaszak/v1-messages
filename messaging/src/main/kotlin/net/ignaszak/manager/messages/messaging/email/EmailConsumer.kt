package net.ignaszak.manager.messages.messaging.email

import net.ignaszak.manager.commons.messaging.message.EmailMessage
import net.ignaszak.manager.messages.servicecontract.EmailService
import net.ignaszak.manager.messages.messaging.email.mapper.EmailMapper
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component

@Component
class EmailConsumer(
    private val emailMapper: EmailMapper,
    private val emailService: EmailService
) {

    @RabbitListener(queues = ["#{@emailQueue.name}"])
    fun receiveMessage(@Payload emailMessage: EmailMessage) {
        val emailModel = emailMapper.messageToModel(emailMessage)
        emailService.send(emailModel)
    }
}