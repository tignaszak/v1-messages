package net.ignaszak.manager.messages.messaging.email

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class EmailConfiguration(private val managerProperties: ManagerProperties) {

    @Bean
    open fun emailQueue(): Queue {
        return Queue(managerProperties.jms.queues["email"]?.queue, false)
    }

    @Bean
    open fun messagesEmailExchange(): TopicExchange {
        return TopicExchange(managerProperties.jms.queues["email"]?.exchange)
    }

    @Bean
    open fun emailQueueToExchangeBinding(queue: Queue, exchange: TopicExchange): Binding {
        return BindingBuilder
            .bind(queue)
            .to(exchange)
            .with(managerProperties.jms.queues["email"]?.routingKey)
    }
}