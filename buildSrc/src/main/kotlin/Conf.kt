object Conf {

    const val LOCAL_MAVEN_REPOSITORY = "maven-repository/"

    const val GROUP = "net.ignaszak"
    const val VERSION = "1.0.0-SNAPSHOT"

    object Versions {
        const val MANAGER_COMMONS = "master-SNAPSHOT"

        const val KOTLIN = "1.7.20"
        const val JAVA = "18"

        const val SPRING_BOOT = "2.7.5"
        const val SPRING_BOOTSTRAP = "3.1.4"
        const val SPRING_CLOUD = "3.1.4"
        const val SPRING_FRAMEWORK = "5.3.23"

        const val DEPENDENCY_MANAGEMENT = "1.1.0"

        const val GROOVY = "4.0.6"
        const val GSON = "2.10"
        const val JACKSON_KOTLIN = "2.13.4"
        const val MAPSTRUCT = "1.5.3.Final"
        const val SNAKEYAML = "1.33"
        const val SPOCK_SPRING = "2.3-groovy-4.0"
        const val TESTCONTAINERS = "1.17.5"
    }

    object Deps {
        // Kotlin
        const val KOTLIN_REFLECT = "org.jetbrains.kotlin:kotlin-reflect"
        const val KOTLIN_STDLIN_JDK8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.KOTLIN}"
        // Spock
        const val GROOVY = "org.apache.groovy:groovy:${Versions.GROOVY}"
        const val SPOCK_CORE = "org.spockframework:spock-core:${Versions.SPOCK_SPRING}"
        // Spring
        const val SPRING_BOOT = "org.springframework.boot:spring-boot:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_AMQP = "org.springframework.boot:spring-boot-starter-amqp:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_MAIL = "org.springframework.boot:spring-boot-starter-mail:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_TEST = "org.springframework.boot:spring-boot-starter-test:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_WEB = "org.springframework.boot:spring-boot-starter-web:${Versions.SPRING_BOOT}"
        const val SPRING_CLOUD_CONFIG_BOOTSTRAP = "org.springframework.cloud:spring-cloud-starter-bootstrap:${Versions.SPRING_BOOTSTRAP}"
        const val SPRING_CLOUD_STARTER_CONFIG = "org.springframework.cloud:spring-cloud-starter-config:${Versions.SPRING_CLOUD}"
        const val SPRING_CLOUD_STARTER_NETFLIX_EUREKA_CLIENT = "org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:${Versions.SPRING_CLOUD}"
        const val SPRING_CONTEXT = "org.springframework:spring-context:${Versions.SPRING_FRAMEWORK}"
        // Commons
        const val MANAGER_COMMONS_CONF = "net.ignaszak:manager-commons-conf:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_ERROR = "net.ignaszak:manager-commons-error:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_MESSAGING = "net.ignaszak:manager-commons-messaging:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_TEST = "net.ignaszak:manager-commons-test:${Versions.MANAGER_COMMONS}"
        // Others
        const val GSON = "com.google.code.gson:gson:${Versions.GSON}"
        const val JACKSON_MODULE_KOTLIN = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.JACKSON_KOTLIN}"
        const val MAPSTRUCT = "org.mapstruct:mapstruct:${Versions.MAPSTRUCT}"
        const val MAPSTRUCT_PROCESSOR = "org.mapstruct:mapstruct-processor:${Versions.MAPSTRUCT}"
        const val SNAKEYAML = "org.yaml:snakeyaml:${Versions.SNAKEYAML}"
        const val TESTCONTAINERS = "org.testcontainers:testcontainers:${Versions.TESTCONTAINERS}"
    }
}