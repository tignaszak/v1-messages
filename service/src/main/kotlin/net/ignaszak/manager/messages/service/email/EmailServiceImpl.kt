package net.ignaszak.manager.messages.service.email

import net.ignaszak.manager.messages.model.EmailModel
import net.ignaszak.manager.messages.servicecontract.EmailService
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

@Service
open class EmailServiceImpl(private val mailSender: JavaMailSender) : EmailService {

    override fun send(email: EmailModel) {
        val mailMessage = SimpleMailMessage()
        mailMessage.setTo(email.to)
        mailMessage.setFrom(email.from)
        mailMessage.setSubject(email.subject)
        mailMessage.setText(email.body)

        mailSender.send(mailMessage)
    }
}